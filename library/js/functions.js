//Add Class based on Touch or NonTouch Device
if ("ontouchstart" in document.documentElement) {
  $('body').addClass('touch');
}
else {
  $('body').addClass('no-touch');
}


$(document).ready(function($){
   function menuFix() {
    if ($(window).width() < 769) {
      $('#menu-primary').prepend($('#menu-secondary').html());
    }
   }
   menuFix();
});

$(document).ready(function($){
  $("input[name=career]:radio").click(function() {
      if($(this).attr("value")=="full") {
          $(".full").slideDown();
          $(".upload").hide();
      }
      if($(this).attr("value")=="upload") {
          $(".full").hide();
          $(".upload").slideDown();
      }
  });
});

//Responsive WP Images
$(document).ready(function($){
    $('.entry img').each(function(){
        $(this).removeAttr('width')
        $(this).removeAttr('height');
    });

  $('.wp-caption').each(function(){
      $(this).css("width", "100%");
  });

});

//Remove Empty WP <p></p> tags
$('p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});


//Smooth Scroll
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


//Parallax (Data Speed)
$('section#headerimg').each(function(){
    var $obj = $(this);

    if ( $(window).width() > 1200 ) {
    $(window).scroll(function() {
      var yPos = -($(window).scrollTop() / $obj.data('speed'));
      var bgpos = '50% '+ yPos + 'px';
      $obj.css('background-position', bgpos );
    });
  }
  });

/*
$(document).ready(function(){
  if($(window).width() > 1024){
     $('#autodownload').get(0).click();
  } else{
  }
});
*/

//Flexslider
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});

//Initialize Mean Menu Mobile Menu
jQuery(document).ready(function () {
    jQuery('header[role="banner"] nav').meanmenu();
});



$(document).ready(function(){

//Wrap Div Around Widget
$( "#text-4" ).wrapInner( "<div class='inner'></div>");

});

$( document ).ready(function($) {
  $('.HeritageImageCarousel').slick({
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 5000,
    fade: true,
    arrows: false
  });
});
