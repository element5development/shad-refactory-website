<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th width="50%">Contact Name</th>
			<th width="50%">Email</th>
		</tr>
	</thead>
	<tbody>
		<?php // Team Contact Query
		$args = array('post_type' => 'teamcontact', 'orderby'=> 'title', 'order' => 'ASC');
		$teamcontact_query = new WP_Query($args);
		while($teamcontact_query->have_posts()) : $teamcontact_query->the_post(); ?>
		<tr id="post-<?php the_ID(); ?>">
			<td width="50%"><?php the_title(); ?></td>
			<td width="50%"><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></td>
		</tr>
		<?php endwhile; wp_reset_postdata(); ?>
	</tbody>
</table>


