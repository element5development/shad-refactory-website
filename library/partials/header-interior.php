
<?php //GET BACKGROUND IMAGE
if ( has_post_thumbnail() ) {
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  $thumb_url = $thumb_url_array[0];
} ?>


<section id="headerimg" data-speed="2" <?php if ( has_post_thumbnail() ) { ?> style="background-image: url('<?php echo $thumb_url ?>');" <?php } ?> >
	
	<div class="wrap">
		<?php if ( is_404() ) : ?>
		<h1>404 Error</h1>
		<?php elseif ( is_search() ) : ?>
		<h1>Search</h1>
		<?php elseif ( is_home() ) : ?>
		<h1>News</h1>
		<?php else : ?>
		<h1><?php the_title(); ?></h1>
		<?php endif; ?>
	</div>

	<div id="bcbar">
		<div class="wrap">
			<?php if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		} ?>
		</div>	
	</div>
	
</section>