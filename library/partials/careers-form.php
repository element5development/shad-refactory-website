<form class="careers">
  <input type="radio" name="career" id="full" value="full"> <span>Fill Out Our Full Application</span>
  <br>
  <input type="radio" name="career" id="upload" value="upload"> <span>Upload Resume</span>
</form> 

<div class="full">
	<?php echo do_shortcode('[gravityform id="2" name="Careers (Full Application)" title="false" description="false"]'); ?>
</div>

<div class="upload">
	<?php echo do_shortcode('[gravityform id="5" name="Careers (Resume Upload)" title="false" description="false"]'); ?>
</div>