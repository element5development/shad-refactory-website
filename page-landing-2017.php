<?php
/**
 * Template Name: Landing 2017 Page
 */
?>

<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
	<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="html5-reset-wordpress-theme">
<!-- Hotjar Tracking Code for http://schadrefractory.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:207401,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">
	<?php
		if (true == of_get_option('meta_author'))
			echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

		if (true == of_get_option('meta_google'))
			echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
	?>
	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/library/images/favicon.png"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--Apple Mobile Web App Metadata-->	
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch4.png" />
	<link rel="apple-touch-startup-image" media="(max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch5.png" />
	<link rel="apple-touch-startup-image" media="(device-width: 375px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6.png">
	<link rel="apple-touch-startup-image" media="(device-width: 414px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6plus.png">
    <link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-landscape.png"  />
    <link rel="apple-touch-startup-image"  media="(device-width: 768px) and (device-height: 1024px) and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-portrait.png"  />
	<link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/library/images/touch-icon.png">
	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/library/css/slick.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/library/css/landing-2017.css" />
	<!-- Modernizer -->
	<script src="<?php echo get_template_directory_uri(); ?>/library/js/modernizr.min.2.8.3.js"></script>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<!--Call Tracking--> 
	<script type="text/javascript"> 
	var google_replace_number="1-800-581-7885";  
	(function(a,e,c,f,g,h,b,d){var k={ak:"947728460",cl:"Ey-_CNjk_GoQzOD0wwM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
	</script>
	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?> onload="_googWcmGet('number', '1-800-581-7885')" >

    <header id="mobile">
    <div class="headertop">
      <div class="wrap">
        <div class="right" style="text-align: right;">
        <span class="call">Give us a call today! <strong><a href="tel:1-800-581-7885" style="color: #e4bf2d;"><span class="number">1-800-581-7885</span></a></strong></span>
        </div>
      </div>
    </div>
    <a href="<?php echo home_url(); ?>" class="logo">Schad Refractory Construction Co.</a>
    <div id="mobilemenu"></div>
  </header>

    <header id="header" role="banner">
      <div class="headertop">
        <div class="wrap">
          <div class="right">
          <span class="call">Give us a call today! <strong><a href="tel:1-800-581-7885"><span class="number">1-800-581-7885</span></a></strong></span>
          <?php wp_nav_menu( array('menu' => 'secondary') ); ?>
          </div>
        </div>
      </div>
      <div class="wrap">
        <a href="<?php echo home_url(); ?>" class="logo">Schad Refractory Construction Co.</a>
        <nav id="nav" role="navigation">
          <?php wp_nav_menu( array('menu' => 'primary') ); ?>
        </nav>
      </div>
    </header>

<!--     <?php //GET BACKGROUND IMAGE
    if ( has_post_thumbnail() ) {
      $thumb_id = get_post_thumbnail_id();
      $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
      $thumb_url = $thumb_url_array[0];
    } ?>

    <section id="headerimg" data-speed="2" <?php if ( has_post_thumbnail() ) { ?> style="background-image: url('<?php echo $thumb_url ?>');" <?php } ?> >
      <div class="wrap">
        <h1><?php the_title(); ?></h1>
      </div>
      <div class="HeritageImageCarousel">
        <?php if( have_rows('header_images') ) {
          while ( have_rows('header_images') ) { the_row(); ?>
            <?php $image = get_sub_field('background_image'); ?>
            <div class="HeritageImageCarousel__item" style="background-image: url('<?php echo $image['url']; ?>')"></div>
          <?php }
        } ?>
      </div>
      <div id="bcbar">
        <div class="wrap">
          <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } ?>
        </div>  
      </div>
    </section> -->

    <?php get_template_part('library/partials/header-interior'); ?>

    <section class="wrap">
      <article class="post">
        <!---
          Content teaser items have two elements, media (image or video) and text.
          These two elements alternate positions automatically on desktop.
          On mobile they collapse into a single column with media always rendered above text.
        -->
        <?php if( have_rows('content_section') ) {
          while ( have_rows('content_section') ) { the_row(); ?>
            <div class="HeritageContentTeaser">
              <div class="HeritageContentTeaser__media">
                <?php the_sub_field('image_or_video'); ?>
              </div>
              <div class="HeritageContentTeaser__content">
                <h1 class="HeritageContentTeaser__title"><?php the_sub_field('title'); ?></h1>
                <div class="HeritageContentTeaser__text">
                  <?php the_sub_field('text_content') ?>
                </div>
              </div>
            </div>
          <?php }
        } ?>
        <div class="full-width">
          <?php the_content(); ?>
        </div>
      </article>
      <?php get_sidebar(); ?>
    </section>

<footer id="footer" class="source-org vcard copyright" role="contentinfo">
  <div id="footertop">
    <div class="wrap">
      <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets') ) : ?><?php endif; ?>
    </div>
  </div>

  <div id="footerbottom">
    <div class="wrap">
      <p>&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?>. All rights reserved.<br><br>
      <a target="_blank" title="Schad Refractory Construction Company BBB Business Review" href="http://www.bbb.org/detroit/business-reviews/refractories/schad-refractory-construction-company-in-detroit-mi-36002164/#bbbonlineclick"><img alt="Schad Refractory Construction Company BBB Business Review" style="border: 0; width: 150px;" src="http://seal-easternmichigan.bbb.org/seals/black-seal-200-65-schad-refractory-construction-company-36002164.png" /></a></p>
      <div class="socialicons">
        <?php echo do_shortcode( '[facebook]') ?>
      </div>
      <a href="https://element5digital.com" target="_blank" class="e5">Website designed by Element5</a>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

<!-- JS -->
<script async src="<?php bloginfo('template_directory'); ?>/library/js/jquery.meanmenu.min.js"></script>
<script async src="<?php bloginfo('template_directory'); ?>/library/js/jquery.flexslider-min.js"></script>
<script async src="<?php bloginfo('template_directory'); ?>/library/js/retina.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/library/js/slick.min.js"></script>
<script async src="<?php bloginfo('template_directory'); ?>/library/js/functions.js"></script>

<!--LinkedIn Ads-->
<script type="text/javascript">
_linkedin_data_partner_id = "91406";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=91406&fmt=gif" />
</noscript>
<!-- End of Custom LP Footer -->	
</body>

</html>

