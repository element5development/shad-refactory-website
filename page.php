<?php get_header(); ?>

<?php get_template_part('library/partials/header-interior'); ?>

<?php if (is_page(543)){ ?>
	<!-- Hidden Automatic Download Triggered by jQuery -->
	<a href="http://schadrefractory.com/wp-content/uploads/2015/09/Schad-Refractory-Brochure.pdf.zip" id="autodownload" style="display: none;">Schad Brochure</a>
<?php } ?>

 <div class="wrap">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="post" id="post-<?php the_ID(); ?>">
			<div class="entry">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; endif; ?>
	<?php get_sidebar(); ?>
</div>


<?php get_footer(); ?>
