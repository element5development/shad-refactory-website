<?php get_header(); ?>
	
<?php get_template_part('library/partials/header-interior'); ?>
	
	<div class="wrap">
	<div class="left">
	<?php if (have_posts()) : ?>
		<h2><?php _e('Search Results','html5reset'); ?></h2>
		<?php post_navigation(); ?>
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><?php the_title(); ?></h2>
				<?php the_date(); ?>
				<div class="entry">
					<?php the_excerpt(); ?>
				</div>
			</article>
		<?php endwhile; ?>
		<?php post_navigation(); ?>
	<?php else : ?>
		<h2><?php _e('Nothing Found','html5reset'); ?></h2>
	<?php endif; ?>
	</div>

<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
