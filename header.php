<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

	<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="html5-reset-wordpress-theme">

<!-- Hotjar Tracking Code for http://schadrefractory.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:207401,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<?php
		if (true == of_get_option('meta_author'))
			echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

		if (true == of_get_option('meta_google'))
			echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
	?>

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/library/images/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--Apple Mobile Web App Metadata-->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch4.png" />
	<link rel="apple-touch-startup-image" media="(max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch5.png" />
	<link rel="apple-touch-startup-image" media="(device-width: 375px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6.png">
	<link rel="apple-touch-startup-image" media="(device-width: 414px)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch6plus.png">
    <link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-landscape.png"  />
    <link rel="apple-touch-startup-image"  media="(device-width: 768px) and (device-height: 1024px) and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" href="<?php echo get_template_directory_uri(); ?>/library/images/launch-ipad-portrait.png"  />
	<link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/library/images/touch-icon.png">

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

	<!-- Modernizer -->
	<script src="<?php echo get_template_directory_uri(); ?>/library/js/modernizr.min.2.8.3.js"></script>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<!--Automatic File Download -->
	<?php if( is_page('Thank you for your interest')) { ?>
		<script>
			window.onload = function(){
  				document.getElementById("downloadLink").click();
			}
		</script>
	<?php } ?>

	<!--Call Tracking-->
	<script type="text/javascript">
	var google_replace_number="1-800-581-7885";
	(function(a,e,c,f,g,h,b,d){var k={ak:"947728460",cl:"Ey-_CNjk_GoQzOD0wwM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
	</script>

	<!--Bing-->
	<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5711240"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5711240&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
	
	<?php wp_head(); ?>

</head>


<body <?php body_class(); ?> onload="_googWcmGet('number', '1-800-581-7885')" >

	<header id="mobile">
		<div class="headertop">
			<div class="wrap">
				<div class="right" style="text-align: right;">
				<span class="call">Give us a call today! <strong><a href="tel:1-800-581-7885" style="color: #e4bf2d;"><span class="number">1-800-581-7885</span></a></strong></span>
				</div>
			</div>
		</div>
		<a href="<?php echo home_url(); ?>" class="logo">Schad Refractory Construction Co.</a>
		<div id="mobilemenu"></div>
	</header>

		<header id="header" role="banner">
			<div class="headertop">
				<div class="wrap">
					<div class="right">
					<span class="call">Give us a call today! <strong><a href="tel:1-800-581-7885"><span class="number">1-800-581-7885</span></a></strong></span>
					<?php wp_nav_menu( array('menu' => 'secondary') ); ?>
					</div>
				</div>
			</div>
			<div class="wrap">
				<a href="<?php echo home_url(); ?>" class="logo">Schad Refractory Construction Co.</a>
				<nav id="nav" role="navigation">
					<?php wp_nav_menu( array('menu' => 'primary') ); ?>
				</nav>
			</div>
		</header>
