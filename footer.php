<footer id="footer" class="source-org vcard copyright" role="contentinfo">
	<div id="footertop">
		<div class="wrap">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets') ) : ?><?php endif; ?>
		</div>
	</div>

	<div id="footerbottom">
		<div class="wrap">
			<p>&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?>. All rights reserved.<br><br>
			<a target="_blank" title="Schad Refractory Construction Company BBB Business Review" href="http://www.bbb.org/detroit/business-reviews/refractories/schad-refractory-construction-company-in-detroit-mi-36002164/#bbbonlineclick"><img alt="Schad Refractory Construction Company BBB Business Review" style="border: 0; width: 150px;" src="http://seal-easternmichigan.bbb.org/seals/black-seal-200-65-schad-refractory-construction-company-36002164.png" /></a></p>
			<div class="socialicons">
				<?php echo do_shortcode( '[facebook]') ?>
			</div>
			<a href="https://element5digital.com" target="_blank" class="e5">Website designed by Element5</a>
		</div>
	</div>
</footer>



<!-- JS -->
<script async src="<?php bloginfo('template_directory'); ?>/library/js/jquery.meanmenu.min.js"></script>
<script async src="<?php bloginfo('template_directory'); ?>/library/js/jquery.flexslider-min.js"></script>
<script async src="<?php bloginfo('template_directory'); ?>/library/js/retina.min.js"></script>
<script async src="<?php bloginfo('template_directory'); ?>/library/js/functions.js"></script>

<!--LinkedIn Ads-->
<script type="text/javascript">
_linkedin_data_partner_id = "91406";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=91406&fmt=gif" />
</noscript>

<?php wp_footer(); ?>

</body>

</html>