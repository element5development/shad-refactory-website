<?php get_header(); ?>

<?php get_template_part('library/partials/header-interior'); ?>

<div class="wrap">
	<div class="left">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<?php if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars(); ?>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
			<?php comments_template(); ?>
		<?php endwhile; endif; ?>
	</div>
	<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>