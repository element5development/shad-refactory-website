<?php

	// Options Framework (https://github.com/devinsays/options-framework-plugin)
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
		require_once dirname( __FILE__ ) . '/library/inc/options-framework.php';
	}

	// Theme Setup (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_setup() {
		load_theme_textdomain( 'html5reset', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );	
		add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
		add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
		register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
		add_theme_support( 'post-thumbnails' );
	}
	add_action( 'after_setup_theme', 'html5reset_setup' );
	
	// Scripts & Styles (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_scripts_styles() {
		global $wp_styles;

		// Load Comments	
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );
	
		// Load Stylesheets
//		wp_enqueue_style( 'html5reset-reset', get_template_directory_uri() . '/reset.css' );
//		wp_enqueue_style( 'html5reset-style', get_stylesheet_uri() );
	
		// Load IE Stylesheet.
//		wp_enqueue_style( 'html5reset-ie', get_template_directory_uri() . '/css/ie.css', array( 'html5reset-style' ), '20130213' );
//		$wp_styles->add_data( 'html5reset-ie', 'conditional', 'lt IE 9' );

		// Modernizr
		// This is an un-minified, complete version of Modernizr. Before you move to production, you should generate a custom build that only has the detects you need.
		// wp_enqueue_script( 'html5reset-modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2.dev.js' );
		
	}
	add_action( 'wp_enqueue_scripts', 'html5reset_scripts_styles' );
	
	// WP Title (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_wp_title( $title, $sep ) {
		global $paged, $page;
	
		if ( is_feed() )
			return $title;
	
//		 Add the site name.
		$title .= get_bloginfo( 'name' );
	
//		 Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
	
//		 Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'html5reset' ), max( $paged, $page ) );
//FIX
//		if (function_exists('is_tag') && is_tag()) {
//		   single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
//		elseif (is_archive()) {
//		   wp_title(''); echo ' Archive - '; }
//		elseif (is_search()) {
//		   echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
//		elseif (!(is_404()) && (is_single()) || (is_page())) {
//		   wp_title(''); echo ' - '; }
//		elseif (is_404()) {
//		   echo 'Not Found - '; }
//		if (is_home()) {
//		   bloginfo('name'); echo ' - '; bloginfo('description'); }
//		else {
//		    bloginfo('name'); }
//		if ($paged>1) {
//		   echo ' - page '. $paged; }
	
		return $title;
	}
	add_filter( 'wp_title', 'html5reset_wp_title', 10, 2 );




//OLD STUFF BELOW


	// Load jQuery
	if ( !function_exists( 'core_mods' ) ) {
		function core_mods() {
			if ( !is_admin() ) {
				wp_deregister_script( 'jquery' );
				wp_register_script( 'jquery', ( "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ), false);
				wp_enqueue_script( 'jquery' );
			}
		}
		add_action( 'wp_enqueue_scripts', 'core_mods' );
	}

	// Clean up the <head>, if you so desire.
	//	function removeHeadLinks() {
	//    	remove_action('wp_head', 'rsd_link');
	//    	remove_action('wp_head', 'wlwmanifest_link');
	//    }
	//    add_action('init', 'removeHeadLinks');

	// Custom Menu
	register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );

	// Widgets
	if ( function_exists('register_sidebar' )) {
		function html5reset_widgets_init() {
			register_sidebar( array(
				'name'          => __( 'Sidebar Widgets', 'html5reset' ),
				'id'            => 'sidebar-primary',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );

			register_sidebar( array(
				'name'          => __( 'Footer Widgets', 'html5reset' ),
				'id'            => 'footer-widgets',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title"><span>',
				'after_title'   => '</span></h3>',
			) );
		}
		add_action( 'widgets_init', 'html5reset_widgets_init' );
	}

	// Navigation - update coming from twentythirteen
	function post_navigation() {
		echo '<div class="navigation">';
		echo '	<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
		echo '	<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
		echo '</div>';
	}

	// Posted On
	function posted_on() {
		printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_author() )
		);
	}

	//Register Custom Post Type
	function slide_post_type() {
	   
	   // Labels
		$labels = array(
			'name' => _x("Slides", "post type general name"),
			'singular_name' => _x("Slide", "post type singular name"),
			'menu_name' => 'Slider',
			'add_new' => _x("Add New", "slide item"),
			'add_new_item' => __("Add New Slide"),
			'edit_item' => __("Edit Slide"),
			'new_item' => __("New Slide"),
			'view_item' => __("View Slide"),
			'search_items' => __("Search Slides"),
			'not_found' =>  __("No Slides Found"),
			'not_found_in_trash' => __("No Slides Found in Trash"),
			'parent_item_colon' => ''
		);
		
		// Register post type
		register_post_type('slide' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),		
 			'taxonomies' => array('category')
		) );
	}
	add_action( 'init', 'slide_post_type', 0 );


	//Register Custom Post Type
	function teamcontacts_post_type() {
	   
	   // Labels
		$labels = array(
			'name' => _x("Team Contacts", "post type general name"),
			'singular_name' => _x("Team Contact", "post type singular name"),
			'menu_name' => 'Team Contacts',
			'add_new' => _x("Add New", "slide item"),
			'add_new_item' => __("Add New Team Contact"),
			'edit_item' => __("Edit Team Contact"),
			'new_item' => __("New Team Contact"),
			'view_item' => __("View Team Contact"),
			'search_items' => __("Search Team Contacts"),
			'not_found' =>  __("No Team Contacts Found"),
			'not_found_in_trash' => __("No Team Contacts Found in Trash"),
			'parent_item_colon' => ''
		);
		
		// Register post type
		register_post_type('teamcontact' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),		
 			'taxonomies' => array('category')
		) );
	}
	add_action( 'init', 'teamcontacts_post_type', 0 );


	// A function to sort by last name.
	function posts_orderby_lastname ($orderby_statement) 
	{
	  $orderby_statement = "RIGHT(post_title, LOCATE(' ', REVERSE(post_title)) - 1) DESC";
	    return $orderby_statement;
	}


	//Replace Excerpt Ellipsis with Permalink
	function new_excerpt_more( $more ) {
	return '...';
	}
	add_filter( 'excerpt_more', 'new_excerpt_more' );

	//Add Shortcode Functionally to Widgets
	add_filter('widget_text', 'do_shortcode');


	// Custom Excerpt
	function excerpt($limit) {
	  $excerpt = explode(' ', get_the_excerpt(), $limit);
	  if (count($excerpt)>=$limit) {
	    array_pop($excerpt);
	    $excerpt = implode(" ",$excerpt).'...';
	  } else {
	    $excerpt = implode(" ",$excerpt);
	  }	
	  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	  return $excerpt;
	}
	 
	function content($limit) {
	  $content = explode(' ', get_the_content(), $limit);
	  if (count($content)>=$limit) {
	    array_pop($content);
	    $content = implode(" ",$content).'...';
	  } else {
	    $content = implode(" ",$content);
	  }	
	  $content = preg_replace('/\[.+\]/','', $content);
	  $content = apply_filters('the_content', $content); 
	  $content = str_replace(']]>', ']]&gt;', $content);
	  return $content;
	}

	//Register Button Shortcode
    function custom_button( $atts, $content = null ) {
    extract(shortcode_atts(array(
    'url'	=> '',
    'target'	=> '',
    'color'	=> '',
    'size'	=> '',
    'align'	=> '',
    'type' 	=> '',
    'download' => '',
    ), $atts));
 
	$color = ($color) ? ' '.$color. '' : '';
	$align = ($align) ? ' '.$align. '' : '';
	$size = ($size) ? ' '.$size. '' : '';
	$target = ($target == '_blank') ? ' target="_blank"' : '';
	$type = ($type) ? ' '.$type. '' : '';
	$target = ($target == 'true') ? ' download' : '';
 
	$out = '<a' .$download.$target. ' class="btn' .$color.$size.$align.$type. '" href="' .$url. '">' .do_shortcode($content). '</a>';
    
    return $out;
	}
	add_shortcode('button', 'custom_button');

	// GREYBOX SHORTCODE
    function grey_container($atts, $content = null) {
      extract( shortcode_atts( array( ), $atts ) );
      return '<div class="grey-container">' . do_shortcode($content) . '</div>';
    }
    add_shortcode('grey-container', 'grey_container');

	//Recent Blog Post Article
	function blog_recentpost_shortcode( $attr ) {
	ob_start();
	get_template_part( 'library/partials/recentpost' );
	return ob_get_clean();
	}
	add_shortcode( 'recentpost', 'blog_recentpost_shortcode' );	


	//Pricing Shortcode
	function careers_shortcode( $attr ) {
	ob_start();
	get_template_part( 'library/partials/careers-form' );
	return ob_get_clean();
	}
	add_shortcode( 'careers_form', 'careers_shortcode' );


	//Team Contacts
	function team_contacts_shortcode( $attr ) {
	ob_start();
	get_template_part( 'library/partials/contact-team' );
	return ob_get_clean();
	}
	add_shortcode( 'teamcontacts', 'team_contacts_shortcode' );	


	//Register Facebook Shortcode
    function facebook_social( $atts ) {
      $html = '<a class="social fb" href="https://www.facebook.com/SchadRefractory" target="_blank"><span>Facebook</span></a>';
          return $html;
    }
	add_shortcode('facebook', 'facebook_social');

	//Register LinkedIn
    function linkedin_social( $atts ) {
      $html = '<a class="social linkedin" href="#" target="_blank">LinkedIn</a>';
          return $html;
    }
	add_shortcode('linkedin', 'linkedin_social');


	//Register Google Plus Shortcode
    function gplus_social( $atts ) {
      $html = '<a class="social gplus" href="#" target="_blank">Google Plus</a>';
          return $html;
    }
	add_shortcode('googleplus', 'gplus_social');


	//Register Map Graphic Shortcode 
    function locations_map( $atts ) {
      $html = '<img src="/wp-content/uploads/2014/12/locations.png" alt="Locations" />';
          return $html;
    }
	add_shortcode('map', 'locations_map');


	//Register FAQ Answer
	function one_half_column($atts, $content = null) {
	   return '<div class="onehalf">' . do_shortcode($content) . '</div>';
	}
	add_shortcode('one_half', 'one_half_column');


	//Register Google Map
    function google_map_detroit( $atts ) {
      $html = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2947.406924714113!2d-83.194892!3d42.376477099999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824cb1050d51a25%3A0xa3192a12627169bd!2s15240+Castleton+St%2C+Detroit%2C+MI+48227!5e0!3m2!1sen!2sus!4v1418846078771" width="100%" height="450" frameborder="0" style="border:0"></iframe>';
          return $html;
    }
	add_shortcode('googlemap_detroit', 'google_map_detroit');


	function google_map_chicago( $atts ) {
      $html = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3006.3345570847005!2d-80.71767468430347!3d41.10538392139195!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8833e4873619588f%3A0x3ac19a749ffdf7e4!2s3700+Oakwood+Ave%2C+Austintown%2C+OH+44515!5e0!3m2!1sen!2sus!4v1457716134271" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
          return $html;
    }
	add_shortcode('googlemap_chicago', 'google_map_chicago');

	function google_map_louisville( $atts ) {
			$html = '<iframe src="https://www.google.com/maps/embed/v1/place?q=3301%20E.%20Luther%20Road%2C%20Floyds%20Knobs%2C%20IN%2047119&key=AIzaSyDEVD8MHkqUCcn1tG7cyqANmpxj6rKTfZI" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
					return $html;
		}
	add_shortcode('googlemap_louisville', 'google_map_louisville');


	//Gravity Forms Tab Index
	add_filter("gform_tabindex", create_function("", "return 4;"));


	//Customize TinyMCE Editor
		function my_theme_add_editor_styles() {
		add_editor_style( 'library/css/custom-editor-style.css' );
		}
		add_action( 'init', 'my_theme_add_editor_styles' );


	// Post Thumbnails
	if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );

	if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'slide', 2000, 750, array( 'center', 'top' ) );
	add_image_size( 'industry', 150, 150, array( 'center', 'top' ) );
	}


?>
