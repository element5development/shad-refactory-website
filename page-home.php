<?php
/**
 * Template Name: Home Page
 */
 get_header(); ?>

<div id="homecontainer">
	 <!-- Slider -->
	<section id="slider">
		<div class="flexslider">
			<ul class="slides">
				<?php // Slide Query
				$args = array('post_type' => 'slide', 'order'=> 'ASC', 'showposts' => -1);
				$slides_query = new WP_Query($args);
				while($slides_query->have_posts()) : $slides_query->the_post(); ?>
				<?php 
				// GET POST ID
				$current_post_id = $post->ID;
				//Use Post Thumb as Background Graphic
				$post_image_id = get_post_thumbnail_id($post_to_use->ID);
				if ($post_image_id) {
					$thumbnail = wp_get_attachment_image_src( $post_image_id, 'slide', false);
					if ($thumbnail) (string)$thumbnail = $thumbnail[0];
				} ?>
				<li style="background: url('<?php echo $thumbnail; ?>') no-repeat center center; background-size: cover;">
					<div class="slidecontent">
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
						<?php if ( $current_post_id == 736 ) { //All Industries Served ?>
							<a class="btn black" href="http://schadrefractory.com/about/" onClick="ga('send', 'event', 'Homepage Slider', 'Link Click', 'About');">See Our History</a>
						<?php } else if ( $current_post_id == 738 ) { //Over 70 Years of Experience ?>
							<a class="btn black" href="http://schadrefractory.com/maintaining-the-highest-safety-standards/" onClick="ga('send', 'event', 'Homepage Slider', 'Link Click', 'Safety Standards');">Learn More</a>
						<?php } else if ( $current_post_id == 740 ) { //Maintaining The Highest Safety Standards  ?>
							<a class="btn black" href="http://schadrefractory.com/services/" onClick="ga('send', 'event', 'Homepage Slider', 'Link Click', 'Services');">See Our Services</a>
						<?php } else if ( $current_post_id == 742 ) { //Maintaining The Highest Safety Standards  ?>
							<a class="btn black" href="http://schadrefractory.com/careers/" onClick="ga('send', 'event', 'Homepage Slider', 'Link Click', 'Careers');">Apply Now</a>
						<?php } else { //no tracking?>

						<?php	} ?>
					</div>
				</li>
			<?php endwhile; wp_reset_postdata(); ?>	
			</ul>
		</div>
	</section>

</div>

<div class="video-block">
	<iframe src="https://player.vimeo.com/video/288622490" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	<h2>Schad's vision is to be a national full-service refractory leader consistently recognized for growth across industries and geographies, creating value for its customers through high-quality and on-time delivery of our services, and providing a safe, 
 challenging, team-oriented work environment for its employees.</h2>
</div>

<!-- Industries -->
<section id="industries">
	<div class="wrap">
		<h2><span>Industries We Work With</span></h2>
		<?php // Industry Child Pages
			$args = array('showposts' => -1, 'post_parent' => 12, 'post_type' => 'page', 'order'=>'ASC');
			$ind_query = new WP_Query($args);
			while($ind_query->have_posts()) : $ind_query->the_post(); ?>
			<a href="<?php the_permalink(); ?>">
				<article class="<?php echo $post->post_name;?>">
					<div class="thumb">
					<?php if ( has_post_thumbnail() ) { ?>
						<?php the_post_thumbnail('industry'); ?>
					<?php } else { ?>
					<img src="<?php bloginfo('template_directory'); ?>/library/images/default-thumb.jpg" alt="<?php the_title(); ?>" />
					<?php } ?>
					<div class="overlay"></div>
					</div>
					<h3><?php the_title(); ?></h3>
					<div class="overlay-mobile"></div>
				</article>
			</a>
		<?php endwhile; wp_reset_postdata(); ?>	
	</div>
</section>

<?php get_footer(); ?>

